/*
 * Testing Outputs
 */

#include <stdio.h>	/* for printf() */
#include <unistd.h>
#include <comedilib.h>

int subdev = 4;		/* change this to your counter subdevice */
int range = 0;		/* more on this later */
int aref = AREF_GROUND;	/* more on this later */

int main(int argc,char *argv[])
{
	comedi_t *it;
	int chan = 0;
	int retval;
	lsampl_t data;

	printf("Test6 - Read Counters\n");

	it = comedi_open("/dev/comedi0");
	if(it == NULL) {
		comedi_perror("comedi_open");
		return 1;
	}

	for(int chani = 0; chani < 2; chani++)
	{
		retval = comedi_data_read(it, subdev, chani, 0, aref, &data);
		if(retval < 0) {
			comedi_perror("comedi_dio_write");
			return 1;
		}
		printf("Ch %d = %d\n", chani, data);
	}

	return 0;
}

