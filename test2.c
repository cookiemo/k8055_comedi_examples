/*
 * Testing Outputs
 */

#include <stdio.h>	/* for printf() */
#include <unistd.h>
#include <comedilib.h>

int subdev = 3;		/* change this to your digital output subdevice */
int chan = 0;		/* change this to your channel */
int range = 0;		/* more on this later */
int aref = AREF_GROUND;	/* more on this later */

int main(int argc,char *argv[])
{
	comedi_t *it;
	int chan = 0;
	int retval;

	it = comedi_open("/dev/comedi0");
	if(it == NULL) {
		comedi_perror("comedi_open");
		return 1;
	}

	for(int bit = 1; bit >= 0; bit--)
	{
		for(int chani = 0; chani < 8; chani++)
		{
			retval = comedi_dio_write(it, subdev, chani, bit);
			if(retval < 0) {
				comedi_perror("comedi_dio_write");
				return 1;
			}
			sleep(1);
		}
	}

	return 0;
}

