# README #

### What is this repository for? ###

This is a repo of examples of using libcomedi to talk to the Velleman K8055 USB Experiment Interface Board.

### How do I get set up? ###

These examples where written on a Debian Sketchy which recognised the board as comedi device without any additonal configuration.
They've been tested using a P8055-1 revision board from 2003.

The intial tests are derived from the code at http://www.comedi.org/doc/writingprograms.html#firstprogram

#### Prerequists ####

- GCC
- Make
- Lib Comedi

On Debian (and presumably Ubuntu) they can be installed with the following command:

apt-get install gcc make libcomedi0 libcomedi-dev

### Contribution guidelines ###

If you create deriviatives or further examples which you think will be useful to others please send pull requests.

### Who do I talk to? ###

Mainainted by James Pritchard <james@phidias.org.uk>
