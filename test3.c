/*
 * Testing Outputs
 */

#include <stdio.h>	/* for printf() */
#include <unistd.h>
#include <comedilib.h>

int subdev = 1;		/* change this to your analog output subdevice */
int chan = 0;		/* change this to your channel */
int range = 0;		/* more on this later */
int aref = AREF_GROUND;	/* more on this later */

int main(int argc,char *argv[])
{
	comedi_t *it;
	int chan = 0;
	int retval;

	it = comedi_open("/dev/comedi0");
	if(it == NULL) {
		comedi_perror("comedi_open");
		return 1;
	}

	for(int chani = 1; chani >= 0; chani--)
	{
		for(int range = 0; range < 255; range++)
		{
			retval = comedi_data_write(it, subdev, chani, 0, 0, range);
			if(retval < 0) {
				comedi_perror("comedi_dio_write");
				return 1;
			}
			usleep(50);
		}
		// Turn them off
		retval = comedi_data_write(it, subdev, chani, 0, 0, 0);
	}

	return 0;
}

