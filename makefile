all: test1 test2 test3 test4 test5 test6

test1:
	gcc -lcomedi -o bin/test1 test1.c

test2:
	gcc -lcomedi -o bin/test2 test2.c

test3:
	gcc -lcomedi -o bin/test3 test3.c

test4:
	gcc -lcomedi -o bin/test4 test4.c

test5:
	gcc -lcomedi -o bin/test5 test5.c

test6:
	gcc -lcomedi -o bin/test6 test6.c

clean:
	rm bin/*
